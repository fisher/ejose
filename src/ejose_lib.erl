%%%-------------------------------------------------------------------
%%% @author Serge Ribalchenko <serge.fisher@gmail.com>
%%% @copyright (C) 2017, Serge Ribalchenko
%%% @doc
%%%       library functions used in ejose
%%% @end
%%% Created : 20 Jul 2017 by Serge Ribalchenko <serge.fisher@gmail.com>
%%%-------------------------------------------------------------------
-module(ejose_lib).

%% API
-export([
         base64url/1,
         debase64url/1,
         strip_b64/1
        ]).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%%      Returns URL safe base64 encoded data, unpadded (RFC7515)
%% @end
%%--------------------------------------------------------------------
-spec base64url( Data :: binary() ) -> Encoded :: binary().
base64url(Data) ->
    Encoded = base64:encode(Data),
    traverse2url(Encoded, <<>>).

%%--------------------------------------------------------------------
%% @doc
%%      Returns URL safe base64 encoded data, unpadded (RFC7515)
%% @end
%%--------------------------------------------------------------------
-spec debase64url( Encoded :: binary() ) -> Data :: binary().
debase64url(Encoded) ->
    base64:decode(b64traverse(Encoded, <<>>)).

%%--------------------------------------------------------------------
%% @doc
%%      remove trailing equal signs after base64 module
%% @end
%%--------------------------------------------------------------------
strip_b64(B) when is_binary(B) ->
  strip(B, erlang:byte_size(B) - 1).

strip(_B, -1) ->
  <<>>;
strip(B, Idx) ->
  case binary:at(B, Idx) of
    61 -> strip(B, Idx - 1);
    _ -> binary:part(B, 0, Idx + 1)
  end.

%%%===================================================================
%%% Internal functions
%%%===================================================================

traverse2url(<<>>, Acc) ->
    Acc;
traverse2url(<<"=",_T/binary>>, Acc) ->
    Acc;
traverse2url(<<"+", T/binary>>, Acc) ->
    traverse2url(T, <<Acc/binary, "-">>);
traverse2url(<<"/", T/binary>>, Acc) ->
    traverse2url(T, <<Acc/binary, "_">>);
traverse2url(<<H,T/binary>>, Acc) ->
    traverse2url(T, <<Acc/binary, H>>).

b64traverse(<<>>, Acc) ->
    b64padding(Acc);
b64traverse(<<"=", _T/binary>>, Acc) ->
    b64padding(Acc);
b64traverse(<<"-", T/binary>>, Acc) ->
    b64traverse(T, <<Acc/binary, "+">>);
b64traverse(<<"_", T/binary>>, Acc) ->
    b64traverse(T, <<Acc/binary, "/">>);
b64traverse(<<H, T/binary>>, Acc) ->
    b64traverse(T, <<Acc/binary, H>>).

%% add trailing equal signs for base64
b64padding(Base64) ->
    case size(Base64) rem 4 of
        0 ->
            Base64;
        %% Remainder ->
        %%     %% form Base64 ++ (4 - Remainder)*"="
        1 ->
            << Base64/binary, 61, 61, 61 >>;
        2 ->
            << Base64/binary, 61, 61 >>;
        3 ->
            << Base64/binary, 61 >>
    end.


