%%%-------------------------------------------------------------------
%%% @author Serge Rybalchenko <serge.fisher@gmail.com>
%%% @copyright (C) 2017, Serge Rybalchenko
%%% @doc
%%%      function subset related to JWE
%%%      Creating new JWE object is defined step by step in RFC7516
%%% @end
%%% Created : 21 Jul 2017 by Serge Rybalchenko <serge.fisher@gmail.com>
%%%-------------------------------------------------------------------
-module(ejose_jwe).

%% API
-export([
         encrypt/2,
         encrypt/3,
         decrypt/2
        ]).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%%    creates new JWE object (in compact serialization)
%%    using predefined shared key (MUST be 256bit for aes)
%% @end
%%--------------------------------------------------------------------
-spec encrypt(
        Msg :: binary(),
        Secret :: binary()
       ) ->
                     JWEobject :: binary().
encrypt(Msg, Secret) ->
    case erlang:system_info(otp_release) of
        "18" ->
            Msg;
        _ ->
            encrypt(Msg, Secret, a256kw)
    end.

%%--------------------------------------------------------------------
%% @doc
%%    decrypts JWE object (from compact serizlization)
%%    using predefined shared key (MUST be 256bit for AES)
%% @end
%%--------------------------------------------------------------------
-spec decrypt(
        JWEobject :: binary(),
        Secret :: binary()
       ) ->
                     DecryptedMessage :: binary()
                                       | {error, Reason :: any()}.
decrypt(NonBinary, _Secret)
  when not is_binary(NonBinary) ->
    {error, {expected_binary_JWE, NonBinary}};
decrypt(JWEobject, Secret) ->
    case erlang:system_info(otp_release) of
        "18" ->
            JWEobject;
        _ ->
            case binary:split(JWEobject, <<46>>, [global]) of
                [AAD, JWEEK, IV, CipherText, AT] ->
                    DecodedHeader = ejose_lib:debase64url(AAD),
                    Header = jiffy:decode(DecodedHeader, [return_maps]),
                    decrypt(Header, AAD, JWEEK, IV, CipherText, AT, Secret);
                [_, _, _] ->
                    {error, {seems_like_unenctypted_token, JWEobject}};
                _ ->
                    {error, garbage_token}
            end
    end.

%%%===================================================================
%%% Internal functions
%%%===================================================================


%%--------------------------------------------------------------------
%% @doc
%%    decrypts JWE object
%% @end
%%--------------------------------------------------------------------
-spec decrypt(
        JWEProtectedHeader :: map(),
        AAD :: binary(),
        JWEEncryptedKey :: binary(),
        InitializationVector :: binary(),
        CipherText :: binary(),
        AuthenticationTag :: binary(),
        Secret :: binary()) ->
                     DecryptedMessage :: binary() |
                                         {error, Reason :: any()}.
decrypt(#{ <<"alg">> := <<"A256KW">>, <<"enc">> := <<"A256GCM">>},
        AAD, JWEEK, IV, CT, AT, Secret) ->
    CipherText = ejose_lib:debase64url(CT),
    AuthenticationTag = ejose_lib:debase64url(AT),
    InitializationVector = ejose_lib:debase64url(IV),
    JWEEncryptedKey = ejose_lib:debase64url(JWEEK),
    case key_unwrap(JWEEncryptedKey, Secret) of
        {error, Reason} ->
            {error, {wrong_secret, Reason}};
        CEK ->
            Result =
                crypto:block_decrypt(aes_gcm, CEK, InitializationVector,
                                     {AAD, CipherText, AuthenticationTag}),
            Result
    end;
decrypt(Header, _, _, _, _, _, _) ->
    {error, {unknown_alg, Header}}.

encrypt(Msg, Secret, rsa) ->
    %% {ok, Bin} = file:read_file("/tmp/mykey.public"),
    %% Key = public_key:pem_decode(Bin),
    create_rsa_oaep(Msg, Secret);
%% A256KW (RFC7518) AES Key Wrap with default initial value using 256-bit key
%% default initial value (RFC3394) IV = A6A6A6A6A6A6A6A6
encrypt(Msg, Secret, a256kw) ->
    create_a256kw(Msg, Secret).

create_a256kw(Msg, Secret) ->
    JWEProtectedHeader =
        #{<<"alg">> => <<"A256KW">>,
          <<"enc">> => <<"A256GCM">>},
    AAD = ejose_lib:base64url(jiffy:encode(JWEProtectedHeader)),
    CEK = crypto:strong_rand_bytes(32), %% Content Encryption Key
    JWEEncryptedKey =
        key_wrap(CEK, Secret),
    JWEEK = ejose_lib:base64url(JWEEncryptedKey),
    InitializationVector = crypto:strong_rand_bytes(16),
    IV = ejose_lib:base64url(InitializationVector),
    {EncryptedBlob, AuthenticationTag} =
        crypto:block_encrypt(aes_gcm, CEK, InitializationVector,
                             {AAD, Msg, 16}),
    CipherText = ejose_lib:base64url(EncryptedBlob),
    AT = ejose_lib:base64url(AuthenticationTag),
    << AAD/binary, 46,
       JWEEK/binary, 46,
       IV/binary, 46,
       CipherText/binary, 46,
       AT/binary >>.


%%--------------------------------------------------------------------
%% @doc
%%   RFC3394
%%   The AES key wrap algorithm is designed to wrap or encrypt key data.
%%   The key wrap operates on blocks of 64 bits.  Before being wrapped,
%%   the key data is parsed into n blocks of 64 bits.
%% @end
%%--------------------------------------------------------------------
%% Inputs:      Plaintext, n 64-bit values {P1, P2, ..., Pn}, and
%%              Key, K (the KEK).
%% Outputs:     Ciphertext, (n+1) 64-bit values {C0, C1, ..., Cn}.
-spec key_wrap(
        PlainText :: binary(),
        KEK :: binary()) ->
                      CipherText :: binary().
key_wrap(PlainText, KEK)
  when (byte_size(PlainText) rem 8) == 0
       andalso bit_size(KEK) == 256 ->
    IV = << 16#A6A6A6A6A6A6A6A6:1/unsigned-big-integer-unit:64 >>,
    Buffer = << IV/binary,
                PlainText/binary >>,
    BlockCount = (byte_size(Buffer) div 8) - 1,
    key_wrap(Buffer, 0, BlockCount, KEK).

key_wrap(Buffer, 6, _BlockCount, _KEK) ->
    Buffer;
key_wrap(Buffer, J, BlockCount, KEK) ->
    key_wrap(
      wrap_helper(Buffer, J, 1, BlockCount, KEK),
      J + 1, BlockCount, KEK).

wrap_helper(Buffer, _J, I, BlockCount, _KEK)
  when I > BlockCount ->
    Buffer;
wrap_helper(Buffer, J, I, BlockCount, KEK) ->
    << A0:8/binary, Rest/binary >> = Buffer,
    HeadSize = (I -1) *8,
    << Head:HeadSize/binary,
       B0:8/binary,
       Tail/binary >> = Rest,
    Round = (BlockCount *J) +I,
    Data = << A0/binary, B0/binary >>,
    << A1:1/unsigned-big-integer-unit:64, B1/binary >> =
        << <<(crypto:block_encrypt(aes_ecb, KEK, Block))/binary >> ||
            << Block:128/bitstring >> <= Data >>,
    A2 = A1 bxor Round,
    wrap_helper(
      << A2:1/unsigned-big-integer-unit:64,
         Head/binary, B1/binary, Tail/binary >>,
      J, I + 1, BlockCount, KEK).



%%--------------------------------------------------------------------
%% @doc
%%   RFC3394
%%   The AES key wrap algorithm reversed backward
%% @end
%%--------------------------------------------------------------------
-spec key_unwrap(
        Encrypted :: binary(),
        KEK :: binary()) ->
                        Decrypted :: binary() |
                                     {error, Reason :: any()}.
key_unwrap(Encrypted, _KEK)
  when (byte_size(Encrypted) rem 8) /= 0 ->
    {error, wrong_ciphertext_len};
key_unwrap(_Encrypted, KEK)
  when (bit_size(KEK) /= 256) ->
    {error, expected_256bit_key};
key_unwrap(Encrypted, KEK) ->
    Blocks = (byte_size(Encrypted) div 8) -1,
    case key_unwrap(Encrypted, 5, Blocks, KEK) of
        << 16#A6A6A6A6A6A6A6A6:1/unsigned-big-integer-unit:64,
           Decrypted/binary >> ->
            Decrypted;  %% hurray! we did it!
        _Oth ->
            {error, {key_unwrap, {Encrypted, KEK}}}
    end.

key_unwrap(Buffer, J, _BC, _KEK)
  when J < 0 ->
    Buffer;
key_unwrap(Buffer, J, BC, KEK) ->
    key_unwrap(
      unwrap_helper(Buffer, J, BC, BC, KEK), J -1, BC, KEK).

unwrap_helper(Buffer, _J, I, _BC, _KEK)
  when I < 1 ->
    Buffer;
unwrap_helper(Buffer, J, I, BC, KEK) ->
    << A0:1/unsigned-big-integer-unit:64,
       Tail/binary >> = Buffer,
    HdSize = (I -1) *8,
    << Hd:HdSize/binary, B0:8/binary, Rest/binary >> = Tail,
    Round = (BC *J) +I,
    A1 = A0 bxor Round,
    Data = << A1:1/unsigned-big-integer-unit:64, B0/binary >>,
    << A2:8/binary, B1/binary >> =
        crypto:block_decrypt(aes_ecb, KEK, Data),
    unwrap_helper(
      <<A2/binary, Hd/binary, B1/binary, Rest/binary>>,
      J, I -1, BC, KEK).


%%--------------------------------------------------------------------
%% @doc
%%     To generate new RSA keypair:
%%      openssl genrsa > /tmp/mykey.private
%%      openssl rsa -in /tmp/mykey.private -pubout >/tmp/mykey.public
%%     To read a public key:
%%      {ok, Bin} = file:read_file("/tmp/mykey.public"),
%%      Key = public_key:pem_decode(Bin).
%% @end
%%--------------------------------------------------------------------

-spec create_rsa_oaep(Msg :: binary(),
                      RcptPubkey :: public_key:rsa_public_key()) ->
                             JWE :: binary() |
                                    {error, Reason :: any()}.
create_rsa_oaep(Msg, RcptPubkey) ->
    JWEProtectedHeader =
        #{<<"alg">> => <<"RSA-OAEP">>,
          <<"enc">> => <<"A256GCM">>},
    AAD = ejose_lib:base64url(jiffy:encode(JWEProtectedHeader)),
    CEK = crypto:strong_rand_bytes(32), %% Content Encryption Key
    JWEEncryptedKey =
        public_key:encrypt_public(CEK, RcptPubkey), %% not sure if RSAES-OAEP
    JWEEK = ejose_lib:base64url(JWEEncryptedKey),
    InitializationVector = crypto:strong_rand_bytes(16),
    IV = ejose_lib:base64url(InitializationVector),
    {EncryptedBlob, AuthenticationTag} =
        crypto:block_encrypt(aes_gcm, CEK, InitializationVector,
                             {AAD, Msg, 16}),
    CipherText = ejose_lib:base64url(EncryptedBlob),
    AT = ejose_lib:base64url(AuthenticationTag),
    << AAD/binary, 46,
       JWEEK/binary, 46,
       IV/binary, 46,
       CipherText/binary, 46,
       AT/binary >>.
