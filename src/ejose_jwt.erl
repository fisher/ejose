%%%-------------------------------------------------------------------
%%% @author Serge Ribalchenko <serge.fisher@gmail.com>
%%% @copyright (C) 2017, Serge Ribalchenko
%%% @doc
%%%       functions to operate on JWT
%%% @end
%%% Created : 19 Jul 2017 by Serge Ribalchenko <serge.fisher@gmail.com>
%%%-------------------------------------------------------------------
-module(ejose_jwt).

%% API
-export([
         create/3,
         create/2,
         verify/2
        ]).

%%%===================================================================
%%% API
%%%===================================================================

create(Payload, Secret) ->
    create(Payload, Secret, hs512).

%%--------------------------------------------------------------------
%% @doc
%%      creates new JWT with given Payload and Secret using Method
%% @end
%%--------------------------------------------------------------------
-spec create(
        Payload :: map(),
        Secret  :: binary(),
        Method  :: hs256 | hs512
       ) ->
                        JWT :: binary()
                             | {error, Reason :: any()}.
create(Payload, Secret, hs256) ->
    new_jwt(Payload, Secret, <<"HS256">>, sha256);
create(Payload, Secret, hs512) ->
    new_jwt(Payload, Secret, <<"HS512">>, sha512).


%%--------------------------------------------------------------------
%% @doc
%%      checks the JWT with given Secret, return Payload on success
%% @end
%%--------------------------------------------------------------------
verify(NonBinary, _Secret)
  when not is_binary(NonBinary) ->
    {error, {wrong_jwt, NonBinary}};
verify(JWT, Secret) ->
    [H, P, S] = binary:split(JWT, <<46>>, [global]),
    DecodedHeader = ejose_lib:debase64url(H),
    Header = jiffy:decode(DecodedHeader, [return_maps]),
    JWT0 = <<H/binary, 46, P/binary>>,
    DecodedSign = ejose_lib:debase64url(S),
    Signature =
        case Header of
            #{ <<"alg">> := <<"HS256">> } ->
                crypto:hmac(sha256, Secret, JWT0);
            #{ <<"alg">> := <<"HS512">> } ->
                crypto:hmac(sha512, Secret, JWT0);
            _ -> <<>>
        end,
    case Signature of
        DecodedSign ->
            %% decode payload and return all the claims
            DecodedPayload = ejose_lib:debase64url(P),
            Payload = jiffy:decode(DecodedPayload, [return_maps]),
            {ok, Payload};
        _ ->
            %% signature is not valid
            {error, invalid_signature}
    end.

%%%===================================================================
%%% Internal functions
%%%===================================================================

new_jwt(Payload, Secret, JOSEMethodName, ErlMethodName) ->
    Header = #{<<"alg">> => JOSEMethodName,
               <<"typ">> => <<"JWT">> },
    EncodedHeader = jiffy:encode(Header),
    EncodedPayload = jiffy:encode(Payload),
    JWT0 = << (ejose_lib:base64url(EncodedHeader))/binary, 46,
              (ejose_lib:base64url(EncodedPayload))/binary >>,
    Sign = crypto:hmac(ErlMethodName, Secret, JWT0),
    << JWT0/binary, 46, (ejose_lib:base64url(Sign))/binary >>.
